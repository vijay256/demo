package com.chatak.transit.afcs.server.enums;

public enum ExportType {

	XLS, PDF, CSV
}
