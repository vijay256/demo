package com.chatak.transit.afcs.server.pojo.web;

import java.util.List;

public class RouteNameList {

	private List<RouteSearchRequest> listOfRouteName;

	public List<RouteSearchRequest> getListOfRouteName() {

		return listOfRouteName;
	}

	public void setListOfRouteName(List<RouteSearchRequest> listOfRouteName) {
		this.listOfRouteName = listOfRouteName;
	}

}
