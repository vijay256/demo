package com.chatak.transit.afcs.server.pojo.web;

import java.util.List;

public class DeviceModelList {

	private List<DeviceModelRequest> listOfDeviceModel;

	public List<DeviceModelRequest> getListOfDeviceModel() {
		return listOfDeviceModel;
	}

	public void setListOfDeviceModel(List<DeviceModelRequest> listOfDeviceModel) {
		this.listOfDeviceModel = listOfDeviceModel;
	}

}
