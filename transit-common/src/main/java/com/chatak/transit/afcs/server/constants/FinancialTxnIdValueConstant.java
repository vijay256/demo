package com.chatak.transit.afcs.server.constants;

public class FinancialTxnIdValueConstant {

	private FinancialTxnIdValueConstant() {

		// Do nothing
	}

	public static final int VALUE_00 = 0;
	public static final int VALUE_01 = 1;
	public static final int VALUE_02 = 2;
	public static final int VALUE_03 = 3;
	public static final int VALUE_04 = 4;
	public static final int VALUE_05 = 5;
	public static final int VALUE_06 = 6;
	public static final int VALUE_07 = 7;
	public static final int VALUE_08 = 8;
	public static final int VALUE_09 = 9;
	public static final int VALUE_10 = 10;
	public static final int VALUE_11 = 11;
	public static final int VALUE_12 = 12;
	public static final int VALUE_13 = 13;
	public static final int VALUE_14 = 14;
	public static final int VALUE_15 = 15;
	public static final int VALUE_16 = 16;
	public static final int VALUE_17 = 17;
	public static final int VALUE_18 = 18;
	public static final int VALUE_19 = 19;
	public static final int VALUE_20 = 20;
	public static final int VALUE_21 = 21;
	public static final int VALUE_22 = 22;
	public static final int VALUE_23 = 23;
	public static final int VALUE_24 = 24;
	public static final int VALUE_25 = 25;
	public static final int VALUE_26 = 26;
	public static final int VALUE_27 = 27;
	public static final int VALUE_28 = 28;
	public static final int VALUE_29 = 29;
	public static final int VALUE_30 = 30;
	public static final int VALUE_31 = 31;
	public static final int VALUE_32 = 32;
	public static final int VALUE_33 = 33;
	public static final int VALUE_34 = 34;
	public static final int VALUE_35 = 35;
	public static final int VALUE_36 = 36;
	public static final int VALUE_37 = 37;
	public static final int VALUE_38 = 38;
	public static final int VALUE_39 = 39;
	public static final int VALUE_40 = 40;
	public static final int VALUE_41 = 41;
	public static final int VALUE_42 = 42;
	public static final int VALUE_43 = 43;
	public static final int VALUE_44 = 44;
	public static final int VALUE_45 = 45;
	public static final int VALUE_46 = 46;
	public static final int VALUE_47 = 47;
	public static final int VALUE_48 = 48;
	public static final int VALUE_49 = 49;

}
