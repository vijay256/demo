package com.chatak.transit.afcs.server.pojo.web;

import java.util.List;

public class VehicleTypeList {

	private List<VehicleTypeSearchRequest> listOfVehicleType;

	public List<VehicleTypeSearchRequest> getListOfVehicleType() {
		return listOfVehicleType;
	}

	public void setListOfVehicleType(List<VehicleTypeSearchRequest> listOfVehicleType) {
		this.listOfVehicleType = listOfVehicleType;
	}

}
