package com.chatak.transit.afcs.server.pojo.web;

import java.util.List;

public class VehicleManufacturerListResponse {

	private List<VehicleManufacturerResponse> listOfManufacturer;

	public List<VehicleManufacturerResponse> getListOfManufacturer() {
		return listOfManufacturer;
	}

	public void setListOfManufacturer(List<VehicleManufacturerResponse> listOfManufacturer) {
		this.listOfManufacturer = listOfManufacturer;
	}

}
