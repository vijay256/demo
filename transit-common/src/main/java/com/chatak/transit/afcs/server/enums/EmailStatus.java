package com.chatak.transit.afcs.server.enums;

/**
 *
 * Enums for holding Email Status
 */
public enum EmailStatus {
  SENT, PENDING, INPROGRESS, FAILURE
}
