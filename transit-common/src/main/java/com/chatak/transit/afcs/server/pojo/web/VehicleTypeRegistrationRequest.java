package com.chatak.transit.afcs.server.pojo.web;

public class VehicleTypeRegistrationRequest {

	private String vehicleType;
	
	private String description;

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
