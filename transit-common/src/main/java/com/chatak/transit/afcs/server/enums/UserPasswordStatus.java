package com.chatak.transit.afcs.server.enums;

public enum UserPasswordStatus {
	FIRSTTIMELOGIN,
	ACTIVE,
	RESETPASSWORD,
	EXPIRED
}
