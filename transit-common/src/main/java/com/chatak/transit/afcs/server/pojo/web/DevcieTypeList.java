package com.chatak.transit.afcs.server.pojo.web;

import java.util.List;

public class DevcieTypeList {

	private List<DeviceTypeSearchRequest> listOfDeviceType;

	public List<DeviceTypeSearchRequest> getListOfDeviceType() {
		return listOfDeviceType;
	}

	public void setListOfDeviceType(List<DeviceTypeSearchRequest> listOfDeviceType) {
		this.listOfDeviceType = listOfDeviceType;
	}
	
}
