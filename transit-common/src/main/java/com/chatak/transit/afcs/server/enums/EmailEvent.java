package com.chatak.transit.afcs.server.enums;

public enum EmailEvent {
	EMPLOYEE_REGISTER,
	USER_REGISTER,
	RESET_PASSWORD
}
