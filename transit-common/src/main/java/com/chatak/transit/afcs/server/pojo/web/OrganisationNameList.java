package com.chatak.transit.afcs.server.pojo.web;

import java.util.List;

public class OrganisationNameList {

	private List<String> listOfOrganisationName;

	public List<String> getListOfOrganisationName() {
		return listOfOrganisationName;
	}

	public void setListOfOrganisationName(List<String> listOfOrganisationName) {
		this.listOfOrganisationName = listOfOrganisationName;
	}

}
