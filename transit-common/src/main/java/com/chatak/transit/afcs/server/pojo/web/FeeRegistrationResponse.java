package com.chatak.transit.afcs.server.pojo.web;

public class FeeRegistrationResponse extends WebResponse {

	private static final long serialVersionUID = 1L;
	
	private String feeId;

	public String getFeeId() {
		return feeId;
	}

	public void setFeeId(String feeId) {
		this.feeId = feeId;
	}

}
