package com.chatak.transit.afcs.server.pojo.web;

import java.util.List;

public class StageNameList {
	
	private List<StageSearchRequest> listOfStageNames;

	public List<StageSearchRequest> getListOfStageNames() {
		return listOfStageNames;
	}

	public void setListOfStageNames(List<StageSearchRequest> listOfStageNames) {
		this.listOfStageNames = listOfStageNames;
	}

}
