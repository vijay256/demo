package com.chatak.transit.afcs.server.constants;

public final class AdminSessionConstants {

	private AdminSessionConstants() {

		// Do nothing
	}

	public static final int ADMIN_SESSION_USER_ID_INITIAL_INDEX = 0;
	public static final int ADMIN_SESSION_USER_ID_FINAL_INDEX = 10;

	public static final int ADMIN_SESSION_EMPLOYEE_ID_INITIAL_INDEX = 10;
	public static final int ADMIN_SESSION_EMPLOYEE_ID_FINAL_INDEX = 20;

	public static final int ADMIN_SESSION_PTO_ID_INITIAL_INDEX = 20;
	public static final int ADMIN_SESSION_PTO_ID_FINAL_INDEX = 32;

	public static final int ADMIN_SESSION_DEVICE_ID_INITIAL_INDEX = 32;
	public static final int ADMIN_SESSION_DEVICE_ID_FINAL_INDEX = 44;

	public static final int ADMIN_SESSION_TRANSACTION_ID_INITIAL_INDEX = 44;
	public static final int ADMIN_SESSION_TRANSACTION_ID_FINAL_INDEX = 48;

	public static final int DATE_TIME_INITIAL_INDEX = 48;
	public static final int DATE_TIME_FINAL_INDEX = 67;
	
	public static final int SOFTWARE_VERSION_CHECK_DATA_LENGTH = 128;

	public static final int SOFT_VER_CHECK_PTOOPER_ID_INITIAL_INDEX = 0;
	public static final int SOFT_VER_CHECK_PTOOPER_ID_FINAL_INDEX = 12;

	public static final int SOFT_VER_CHECK_EQP_ID_INITIAL_INDEX = 12;
	public static final int SOFT_VER_CHECK_EQP_ID_FINAL_INDEX = 24;

	public static final int SOFT_VER_CHECK_SOFT_ID_INITIAL_INDEX = 24;
	public static final int SOFT_VER_CHECK_SOFT_ID_FINAL_INDEX = 30;

	public static final int SOFT_VER_CHECK_SOFT_VER_INITIAL_INDEX = 30;
	public static final int SOFT_VER_CHECK_SOFT_VER_FINAL_INDEX = 42;

	public static final int SOFT_VER_CHECK_TXN_ID_INITIAL_INDEX = 42;
	public static final int SOFT_VER_CHECK_TXN_ID_FINAL_INDEX = 46;

	public static final int SOFT_VER_CHECK_DATE_TIME_INITIAL_INDEX = 46;
	public static final int SOFT_VER_CHECK_DATE_TIME_FINAL_INDEX = 65;

	public static final int SOFT_VER_CHECK_OPERATOR_ID_INITIAL_INDEX = 65;
	public static final int SOFT_VER_CHECK_OPERATOR_ID_FINAL_INDEX = 75;

	public static final int SOFT_VER_CHECK_RESERVERD_ID_INITIAL_INDEX = 75;
	public static final int SOFT_VER_CHECK_RESERVERD_ID_FINAL_INDEX = 127;

	public static final int SOFT_VER_CHECK_CHECKSUM_INITIAL_INDEX = 127;
	public static final int SOFT_VER_CHECK_CHECKSUM_FINAL_INDEX = 128;

}