package com.chatak.transit.afcs.server.pojo.web;

public class PmOnboardingRequest {

	private String currency;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
