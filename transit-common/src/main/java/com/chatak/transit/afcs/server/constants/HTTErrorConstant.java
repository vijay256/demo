package com.chatak.transit.afcs.server.constants;

public class HTTErrorConstant {

	public static final String CHATAK_AFCS_SERVICE_HTT_TICKET_NO_REQUIRED = "chatak.afcs.service.htt.ticket.no.required";

	public static final String CHATAK_AFCS_SERVICE_HTT_TICKET_NO_INVALID = "chatak.afcs.service.htt.ticket.no.invalid";

	public static final String CHATAK_AFCS_SERVICE_HTT_TXN_ID_REQUIRED = "chatak.afcs.service.transaction.id.required";

	public static final String CHATAK_AFCS_SERVICE_HTT_PTO_OPERATION_ID_REQUIRED = "chatak.afcs.service.ptooperation.id.required";

	public static final String CHATAK_AFCS_SERVICE_HTT_DEVICE_ID_REQUIRED = "chatak.afcs.service.htt.device.id.required";

	public static final String CHATAK_AFCS_SERVICE_HTT_SHIFT_BATCH_NO_REQUIRED = "chatak.afcs.service.htt.shift.batch.no.required";

	public static final String CHATAK_AFCS_SERVICE_HTT_SHIFT_BATCH_NO_INVALID = "chatak.afcs.service.htt.shift.batch.no.invalid";

	public static final String CHATAK_AFCS_SERVICE_HTT_SHIFT_CODE_INVALID = "chatak.afcs.service.htt.shift.code.required";
	
	public static final String CHATAK_AFCS_SERVICE_HTT_CONDUCTOR_EMPLOYEE_ID_INVALID = "chatak.afcs.service.htt.shift.conductor.employee.id.invalid";

	public static final String CHATAK_AFCS_SERVICE_HTT_TICKET_DATE_TIME_REQUIRED = "chatak.afcs.service.htt.ticket.date.time.required";
	
	public static final String CHATAK_AFCS_SERVICE_HTT_TICKET_FARE_REQUIRED = "chatak.afcs.service.htt.ticket.fare.required";
	
	public static final String CHATAK_AFCS_SERVICE_HTT_TICKET_OPERATION_DATE_REQUIRED = "chatak.afcs.service.htt.ticket.operation.date.required";
	
	public static final String CHATAK_AFCS_SERVICE_HTT_TICKET_PAYMENT_MODE_REQUIRED = "chatak.afcs.service.htt.ticket.payment.mode.required";
	
	private HTTErrorConstant() {
		super();
	}
	
}
