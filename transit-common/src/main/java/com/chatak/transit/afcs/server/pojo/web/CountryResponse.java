package com.chatak.transit.afcs.server.pojo.web;

public class CountryResponse {

	private int id;

	private String countryName;

	public int getId() {
		return id;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
}
