package com.chatak.transit.afcs.server.constants;

public class FileDownloadConstant {
	private FileDownloadConstant() {

		// Do nothing
	}

	public static final int FILE_DOWNLOAD_PTO_ID_INITIAL_INDEX = 0;
	public static final int FILE_DOWNLOAD_PTO_ID_FINAL_INDEX = 12;

	public static final int FILE_DOWNLOAD_DEVICE_ID_INITIAL_INDEX = 12;
	public static final int FILE_DOWNLOAD_DEVICE_ID_FINAL_INDEX = 24;

	public static final int FILE_DOWNLOAD_VERSION_INITIAL_INDEX = 24;
	public static final int FILE_DOWNLOAD_VERSION_FINAL_INDEX = 38;

	public static final int FILE_DOWNLOAD_TRANSACTION_ID_INITIAL_INDEX = 38;
	public static final int FILE_DOWNLOAD_TRANSACTION_ID_FINAL_INDEX = 42;

	public static final int DATE_TIME_INITIAL_INDEX = 42;
	public static final int DATE_TIME_FINAL_INDEX = 61;

}
