package com.chatak.transit.afcs.server.pojo.web;

import java.util.List;

public class StopNameList {

	private List<StopSearchRequest> listOfStopNames;

	public List<StopSearchRequest> getListOfStopNames() {
		return listOfStopNames;
	}

	public void setListOfStopNames(List<StopSearchRequest> listOfStopNames) {
		this.listOfStopNames = listOfStopNames;
	}
	
	

}
