package com.chatak.transit.afcs.server.pojo.web;

import java.util.List;

public class VehicleModelList {

	private List<VehicleModelResponse> listOfVehicleModel;

	public List<VehicleModelResponse> getListOfVehicleModel() {
		return listOfVehicleModel;
	}

	public void setListOfVehicleModel(List<VehicleModelResponse> listOfVehicleModel) {
		this.listOfVehicleModel = listOfVehicleModel;
	}
	
}
