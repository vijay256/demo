package com.chatak.transit.afcs.server.pojo.web;

public class UserDetail {
	
	private String userId;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
