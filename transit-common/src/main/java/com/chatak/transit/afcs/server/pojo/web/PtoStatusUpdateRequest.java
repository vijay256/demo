package com.chatak.transit.afcs.server.pojo.web;

public class PtoStatusUpdateRequest extends BaseRequest{

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
