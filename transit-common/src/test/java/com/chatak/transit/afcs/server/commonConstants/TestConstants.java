package com.chatak.transit.afcs.server.commonConstants;

public class TestConstants {

	public static final String SOFTWARE_VERSION = "12345678";

	public static final String USER_ID = "chatak1";

	public static final String ADMIN_USER_ID = "adminUserID";
	
	public static final String TICKET_NO = "ticketNo";
	
	public static final String TRANSACTION_ID = "A001";

	public static final String PTO_OPERATION_ID = "CHATAK000001";

	public static final String BASEPATH = "basepath";

	public static final String CREATEDBY = "createdBy";

	public static final String DESCRIPTION = "description";

	public static final String DEVICE_TYPE = "deviceType";

	public static final String LATEST_VERSION = "version";

	public static final String PTO_ID = "ptoId";

	public static final String SOFTWARE_ID = "1234";

	public static final String SOFTWARE_NAME = "softwareName";

	public static final String STATUS_SUCCESS = "Success";

	public static final String UPDATED_BY = "updatedBy";

	public static final String STATUS_MESSAGE = "status-mesage";

	public static final String DEPOT_NAME = "depotName";

	public static final String STRING_ONE = "1";

	public static final String CITY = "City";

	public static final String ADDRESS = "address";

	public static final String PTO_OPERATION_NOT_EXIST = "PTO Operation does not exist";

	public static final String USER_NOT_EXIST = "User ID does not exist";

	public static final String INVALID_TRANSACTION_ID = "Invalid Transaction ID";

	public static final String EQIPMENT_CAPABILITIES = "deviceCapabilities";

	public static final String DEVICE_TYPE_NAME = "deviceTypeName";
	
	public static final String ROUTE_NAME = "routeName";
	
	public static final String DEVICE_NAME_ALREADY_EXIST ="Device name already exist";

	public static final String DEVICE_ID="CETM13660001";
	
	public static final String TICKET_FARE="40";
	
	public static final String USER_ROLE="01";
	
	public static final String DATE_TIME="1970-11-1 00:00:00";
	
	public static final int INT_ZERO = 0;

	public static final int INT_ONE = 1;

	public static final int INT_TWO = 2;
	
	public static final int INT_THREE = 3;
	
	public static final int INT_FOUR = 4;
	
	public static final int INT_FIVE = 5;
	
	public static final int SIX = 6;
	
	public static final String STRING_SIX = "6";
	
	public static final int INT_SEVEN = 7;
	
	public static final int INT_EIGHT = 8;
	
	public static final int INT_NINE = 9;

	public static final int DEPOT_ID = 100;

	public static final String STATUS_CODE = "0";
	
	public static final String STRING_SIXTEEN = "16";

	public static final int INT_SIXTEEN = 16;
	
	public static final int INT_EIGHTEEN = 18;

	public static final int NINETEEN = 19;
	
	public static final String STRING_TWENTY = "20";
	
	public static final int INT_TWENTY = 20;

	public static final int INT_TWENTY_TWO = 22;
	
	public static final int INT_TWENTY_THREE = 23;
	
	public static final int INT_TWENTY_FOUR = 24;
	
	public static final int INT_TWENTY_FIVE = 25;
	
	public static final String STRING_TWENTY_SIX = "26";
	
	public static final int INT_TWENTY_SIX = 26;
	
	public static final int THIRTY_ONE = 31;
	
	public static final int ROUTE_ID = 1;
	
	public static final long LONG_ONE = 1l;

	public static final long LONG_TWO = 2l;
	
	public static final int MODEL_TYPE_INT_FOURTYFOUR = 44;
	
	public static final String MODEL_TYPE = "DeviceTypeName does not exist"; 
	
	public static final int MODEL_NAME_CODE = 43;
	
	public static final String MODEL_NAME = "Device Model Name Aleready Exist..";
	
    public static final int DEVICE_MODEL_NAME_NOT_EXIST_CODE = 46;
    
    public static final int FIFTY= 50;
	
	public static final String DEVICE_MODEL_NAME_NOT_EXIST = "Device Model Name does not exist";

	public static final Long ID = 3l;
	
	public static final String FINANCIAL_TXN_SHIFTEND = "F001";
	
	public static final String FINANCIAL_TXN_TRIPEND = "F002";
	
	public static final String STOP_CODE = "1001";
	
	public static final String SHIFT_CODE = "1002";
 
	public static final String FINANCIAL_TXN_DATA_REQUEST  = "chatak1   CHATAKOP01CHATAK00000110015003    F0012018-10-19 11:17:25                                                            0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             1     6   0                                                                 00.00.01      01.00.007655c79         i9000S                                                                          6";

	public static final String ADMIN_SESSION_REQUEST ="chatak1   CHATAKOP01CHATAK000001CETM13660001A0012018-08-16 02:16:12                                                            T";
	
	public static final String EMP_ID = "1001";
	
	public static final String DATE_FORMAT = "2018-08-16 02:16:12.0";
	
	public static final String TERMINAL_SETUP_REQUEST = "7655c79         i9000S    2018-08-17 12:55:09                                                                                  P";

	public static final String TICKET_TRANSACTION_DATA_REQUEST  = "0015009776      TK012018-10-20 10:55:28200                                 2018-10-19P0      00010001                          CHATAK00000110015003                        CHATAKOP01          S1    6   T1  03U                   1001                                                                                                                                                        >";
	
	public static final String FINANCIAL_TXN_DATA = "chatak1   CHATAKOP01CHATAK00000110015003    F0012018-10-19 11:17:25                                                            0                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             1     6   0                                                                 00.00.01      01.00.007655c79         i9000S                                                                          6";
	
	public static final String FILE_DOWNLOAD_REQUEST = "CHATAK00000110015003          00.00.00FD012018-09-19 04:34:25                                                                  ?";

	public static final String EMPTY = "";
	
	public static final String PAYMENT_MODE = "paymentMode";
	
	public static final String MODEl_NUMBER = "modelNumber";

	public static final String STATION_CODE = "123";
	
	public static final String EMAIL = "user5@mifare.com";
	
	public static final String USER_NAME = "userName";
	
	private TestConstants() {

	}

}
