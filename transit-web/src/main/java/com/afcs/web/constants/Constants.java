package com.afcs.web.constants;

import com.afcs.web.controller.Properties;

public class Constants {
	
	private Constants() {
		// Do Nothing
	}
	
	public static final String USER_ID = "userId";
	public static final String CHATAK_ID = "CHATAK_ID";
	public static final String DESCRIPTION = "description";
	public static final String PTO_LIST = "ptoList";
	public static final String ORGANIZATION_ID = "organizationId";
	public static final String DEVICE_TYPE_CODE = "deviceTypeCode";
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String SUCCESS_CODE = "0";
	public static final String YES = "yes";
	public static final String NO = "no";
	public static final String PAGE_NUMBER = "pageNumber";
	public static final int ONE = 1;
	public static final String MODEL_ATTRIBUTE_ENTITY_LIST_PAGE_NUMBER = "entityListPageNumber";
	public static final String MODEL_ATTRIBUTE_ENTITY_LIST_BEGIN_PAGE_NUM = "beginEntityPage";
	public static final String MODEL_ATTRIBUTE_ENTITY_LIST_END_PAGE_NUM = "endEntityPage";
	public static final Integer MAX_ENTITIES_PAGINATION_DISPLAY_SIZE = 10;
	public static final Integer MAX_ENTITY_DISPLAY_SIZE = 10;
	public static final String MODEL_ATTRIBUTE_PORTAL_LIST_PAGE_NUMBER = "portalListPageNumber";
	public static final String MODEL_ATTRIBUTE_PORTAL_LIST_BEGIN_PAGE_NUM = "beginPortalPage";
	public static final String MODEL_ATTRIBUTE_PORTAL_LIST_END_PAGE_NUM = "endPortalPage";
	public static final String PORTAL_PAGES_SESSION_NAME = "portalPages";
	public static final Integer MAX_ENTITIES_PORTAL_DISPLAY_SIZE = 10;
	public static final Integer TWO = 2;
	public static final Integer THREE = 3;
	public static final Integer FOUR = 4;
	public static final Integer FIVE = 5;
	public static final Integer SIX = 6;
	public static final Integer SEVEN = 7;
	public static final Integer EIGHT = 8;
	public static final Integer NINE = 9;
	public static final Integer ZERO = 0;
	public static final Integer ELEVEN = 11;
	public static final String TEN = "10";
	public static final String MARKETING_PAGINATION_LIST = "marketingPaginationList";
	public static final String SEARCH_FLAG = "searchFlag";
	public static final boolean FLAG_TRUE = true;
	public static final String EXISTING_FEATURES = "existingFeatures";
	public static final String EXISTING_FEATURE_DATA = "existingFeatureData";
	public static final String LOGIN_RESPONSE = "loginResponse";
	public static final Integer FEATURE_LIST_INDEX_LIST = 4;
	public static final String CURRENT_LOGIN_IP_ADDRESS = "currentLoginIpAddress";
	public static final String ALPHA_CHARS = "MK8A3BNEFV3AIC5K13RFKA3EJF6F9486M48507B5P5DFRK8A3BEF6F9486M48507B5P5DFR";
	public static final String PREFIX = "_crypt_mw_";
	public static final String SUFFIX = "M19A02N20V11I==";
	public static final String PASS_WD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@,£^*?€\"!$]).{8,16})";
	public static final String PTO_LIST_DATA = "ptoListData";
	public static final String ORGANIZATION_LIST = "organizationList";
	public static final String ROLE_LIST = "roleList";
	public static final String USER_TYPE = "userType";
	public static final String ROLE_ID="rollId";
	public static final String PTO_ID="ptoId";
	public static final String ACTIVE = "Active";
	public static final String SUSPENDED = "Suspended";
	public static final String TERMINATED = "Terminated";
	public static final String EXPORT_HEADER_DATE_FORMAT = Properties.getProperty("chatak.reportdate.format");
	public static final String EXPORT_FILE_DATE_FORMAT_MMDDYY = Properties.getProperty("chatak.reportfile.date.format.mmddyy");
	public static final Integer INDEX_TEN = 10;
	public static final Integer INT_THIRTY = 30;
	public static final Integer INT_TWO = 2;
	public static final Integer TWENTY_FIVE = 25;
    public static final Integer INT_TEN = 2;
    public static final Integer SEVEN_HUNDRED_SIXTY_FIVE = 765;
    public static final Integer INDEX_FIFTEEN = 15;
    public static final Integer INDEX_EIGHTEEN = 18;
    public static final Integer INT_FIFTY = 50;
    public static final Integer INDEX_ONE = 1;
    public static final String CHATAK_GENERAL_ERROR = "chatak.general.error";
    public static final String PDF_FILE_FORMAT = "PDF";
    public static final String XLS_FILE_FORMAT = "XLS";
    public static final Integer CARD_NUMBER_MIN_LENGTH = 12;

	public static final String ORG_ID = "orgId";
	public static final String PTO_MASTER_ID = "ptoMasterId";
	public static final String PTO_DATA = "ptoData";
}
