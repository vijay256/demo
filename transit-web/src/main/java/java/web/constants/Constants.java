package java.web.constants;

public class Constants {

	private Constants() {
		// Do Nothing
	}
	
	public static final String USER_ID = "userId";
	public static final String PTO_OPERATION_LIST = "ptoOperationList";
	public static final String GET_PRIVILEGE_LIST = "getPrivilegeList";
	public static final String PTO_ID = "ptoId";
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String SUCCESS_CODE = "0";
	public static final String TRANSACTION_ID = "transactionId";
	public static final String PTO_OPERATION_ID = "ptoOperationIdt";
	
}
