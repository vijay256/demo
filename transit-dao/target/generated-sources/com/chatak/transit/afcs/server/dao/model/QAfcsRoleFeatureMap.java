package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QAfcsRoleFeatureMap is a Querydsl query type for AfcsRoleFeatureMap
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAfcsRoleFeatureMap extends EntityPathBase<AfcsRoleFeatureMap> {

    private static final long serialVersionUID = -18668514L;

    public static final QAfcsRoleFeatureMap afcsRoleFeatureMap = new QAfcsRoleFeatureMap("afcsRoleFeatureMap");

    public final StringPath createdBy = createString("createdBy");

    public final DateTimePath<java.sql.Timestamp> createdDate = createDateTime("createdDate", java.sql.Timestamp.class);

    public final NumberPath<Long> featureId = createNumber("featureId", Long.class);

    public final NumberPath<Long> featureSequenceId = createNumber("featureSequenceId", Long.class);

    public final NumberPath<Long> roleId = createNumber("roleId", Long.class);

    public final StringPath updatedBy = createString("updatedBy");

    public final DateTimePath<java.sql.Timestamp> updatedDate = createDateTime("updatedDate", java.sql.Timestamp.class);

    public final StringPath userType = createString("userType");

    public QAfcsRoleFeatureMap(String variable) {
        super(AfcsRoleFeatureMap.class, forVariable(variable));
    }

    public QAfcsRoleFeatureMap(Path<? extends AfcsRoleFeatureMap> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAfcsRoleFeatureMap(PathMetadata<?> metadata) {
        super(AfcsRoleFeatureMap.class, metadata);
    }

}

