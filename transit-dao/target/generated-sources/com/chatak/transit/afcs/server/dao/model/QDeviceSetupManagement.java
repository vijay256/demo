package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QDeviceSetupManagement is a Querydsl query type for DeviceSetupManagement
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDeviceSetupManagement extends EntityPathBase<DeviceSetupManagement> {

    private static final long serialVersionUID = 417801341L;

    public static final QDeviceSetupManagement deviceSetupManagement = new QDeviceSetupManagement("deviceSetupManagement");

    public final NumberPath<Long> deviceId = createNumber("deviceId", Long.class);

    public final StringPath deviceModel = createString("deviceModel");

    public final StringPath deviceOemId = createString("deviceOemId");

    public final DateTimePath<java.sql.Timestamp> generationDatetime = createDateTime("generationDatetime", java.sql.Timestamp.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.sql.Timestamp> processDateAndtime = createDateTime("processDateAndtime", java.sql.Timestamp.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public QDeviceSetupManagement(String variable) {
        super(DeviceSetupManagement.class, forVariable(variable));
    }

    public QDeviceSetupManagement(Path<? extends DeviceSetupManagement> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDeviceSetupManagement(PathMetadata<?> metadata) {
        super(DeviceSetupManagement.class, metadata);
    }

}

