package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QStopProfile is a Querydsl query type for StopProfile
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QStopProfile extends EntityPathBase<StopProfile> {

    private static final long serialVersionUID = 1499859482L;

    public static final QStopProfile stopProfile = new QStopProfile("stopProfile");

    public final NumberPath<Double> distance = createNumber("distance", Double.class);

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath reason = createString("reason");

    public final NumberPath<Long> routeId = createNumber("routeId", Long.class);

    public final NumberPath<Long> stageId = createNumber("stageId", Long.class);

    public final StringPath status = createString("status");

    public final NumberPath<Long> stopId = createNumber("stopId", Long.class);

    public final StringPath stopName = createString("stopName");

    public final NumberPath<Integer> stopSequenceNumber = createNumber("stopSequenceNumber", Integer.class);

    public QStopProfile(String variable) {
        super(StopProfile.class, forVariable(variable));
    }

    public QStopProfile(Path<? extends StopProfile> path) {
        super(path.getType(), path.getMetadata());
    }

    public QStopProfile(PathMetadata<?> metadata) {
        super(StopProfile.class, metadata);
    }

}

