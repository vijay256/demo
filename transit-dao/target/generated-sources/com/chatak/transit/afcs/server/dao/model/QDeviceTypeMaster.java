package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QDeviceTypeMaster is a Querydsl query type for DeviceTypeMaster
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDeviceTypeMaster extends EntityPathBase<DeviceTypeMaster> {

    private static final long serialVersionUID = -2137059777L;

    public static final QDeviceTypeMaster deviceTypeMaster = new QDeviceTypeMaster("deviceTypeMaster");

    public final StringPath createdBy = createString("createdBy");

    public final DateTimePath<java.sql.Timestamp> createdTime = createDateTime("createdTime", java.sql.Timestamp.class);

    public final StringPath description = createString("description");

    public final NumberPath<Long> deviceTypeId = createNumber("deviceTypeId", Long.class);

    public final StringPath deviceTypeName = createString("deviceTypeName");

    public final StringPath status = createString("status");

    public final StringPath updatedBy = createString("updatedBy");

    public final DateTimePath<java.sql.Timestamp> updatedTime = createDateTime("updatedTime", java.sql.Timestamp.class);

    public QDeviceTypeMaster(String variable) {
        super(DeviceTypeMaster.class, forVariable(variable));
    }

    public QDeviceTypeMaster(Path<? extends DeviceTypeMaster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDeviceTypeMaster(PathMetadata<?> metadata) {
        super(DeviceTypeMaster.class, metadata);
    }

}

