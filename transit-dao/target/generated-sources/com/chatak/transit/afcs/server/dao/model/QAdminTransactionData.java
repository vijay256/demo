package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QAdminTransactionData is a Querydsl query type for AdminTransactionData
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAdminTransactionData extends EntityPathBase<AdminTransactionData> {

    private static final long serialVersionUID = -806368538L;

    public static final QAdminTransactionData adminTransactionData = new QAdminTransactionData("adminTransactionData");

    public final DateTimePath<java.sql.Timestamp> dateTime = createDateTime("dateTime", java.sql.Timestamp.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath password = createString("password");

    public final StringPath transactionId = createString("transactionId");

    public final StringPath userId = createString("userId");

    public QAdminTransactionData(String variable) {
        super(AdminTransactionData.class, forVariable(variable));
    }

    public QAdminTransactionData(Path<? extends AdminTransactionData> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminTransactionData(PathMetadata<?> metadata) {
        super(AdminTransactionData.class, metadata);
    }

}

