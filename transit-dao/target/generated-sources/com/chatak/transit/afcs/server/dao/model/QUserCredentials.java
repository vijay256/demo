package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QUserCredentials is a Querydsl query type for UserCredentials
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QUserCredentials extends EntityPathBase<UserCredentials> {

    private static final long serialVersionUID = 1537677188L;

    public static final QUserCredentials userCredentials = new QUserCredentials("userCredentials");

    public final StringPath address = createString("address");

    public final StringPath adminUserId = createString("adminUserId");

    public final DateTimePath<java.sql.Timestamp> createdDateTime = createDateTime("createdDateTime", java.sql.Timestamp.class);

    public final BooleanPath currentLoginStatus = createBoolean("currentLoginStatus");

    public final StringPath email = createString("email");

    public final StringPath emailToken = createString("emailToken");

    public final StringPath firstName = createString("firstName");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath lastName = createString("lastName");

    public final NumberPath<Long> loginMode = createNumber("loginMode", Long.class);

    public final StringPath organizationId = createString("organizationId");

    public final NumberPath<Long> orgId = createNumber("orgId", Long.class);

    public final StringPath passWord = createString("passWord");

    public final StringPath phone = createString("phone");

    public final StringPath previousPasswords = createString("previousPasswords");

    public final StringPath ptoId = createString("ptoId");

    public final NumberPath<Long> ptoMasterId = createNumber("ptoMasterId", Long.class);

    public final StringPath reason = createString("reason");

    public final StringPath roleName = createString("roleName");

    public final StringPath transactionID = createString("transactionID");

    public final DateTimePath<java.sql.Timestamp> updatedDateTime = createDateTime("updatedDateTime", java.sql.Timestamp.class);

    public final StringPath userID = createString("userID");

    public final StringPath userName = createString("userName");

    public final StringPath userRole = createString("userRole");

    public final StringPath userStatus = createString("userStatus");

    public final StringPath userType = createString("userType");

    public QUserCredentials(String variable) {
        super(UserCredentials.class, forVariable(variable));
    }

    public QUserCredentials(Path<? extends UserCredentials> path) {
        super(path.getType(), path.getMetadata());
    }

    public QUserCredentials(PathMetadata<?> metadata) {
        super(UserCredentials.class, metadata);
    }

}

