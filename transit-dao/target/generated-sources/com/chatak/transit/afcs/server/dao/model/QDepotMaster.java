package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QDepotMaster is a Querydsl query type for DepotMaster
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDepotMaster extends EntityPathBase<DepotMaster> {

    private static final long serialVersionUID = -1399652215L;

    public static final QDepotMaster depotMaster = new QDepotMaster("depotMaster");

    public final DateTimePath<java.sql.Timestamp> createdDateTime = createDateTime("createdDateTime", java.sql.Timestamp.class);

    public final NumberPath<Long> depotId = createNumber("depotId", Long.class);

    public final StringPath depotIncharge = createString("depotIncharge");

    public final StringPath depotMobile = createString("depotMobile");

    public final StringPath depotName = createString("depotName");

    public final StringPath depotShortName = createString("depotShortName");

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath reason = createString("reason");

    public final StringPath status = createString("status");

    public final DateTimePath<java.sql.Timestamp> updatedDateTime = createDateTime("updatedDateTime", java.sql.Timestamp.class);

    public QDepotMaster(String variable) {
        super(DepotMaster.class, forVariable(variable));
    }

    public QDepotMaster(Path<? extends DepotMaster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDepotMaster(PathMetadata<?> metadata) {
        super(DepotMaster.class, metadata);
    }

}

