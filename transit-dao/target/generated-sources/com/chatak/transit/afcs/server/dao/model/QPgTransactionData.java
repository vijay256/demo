package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPgTransactionData is a Querydsl query type for PgTransactionData
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPgTransactionData extends EntityPathBase<PgTransactionData> {

    private static final long serialVersionUID = 410689860L;

    public static final QPgTransactionData pgTransactionData = new QPgTransactionData("pgTransactionData");

    public final NumberPath<Long> authId = createNumber("authId", Long.class);

    public final StringPath cgRefNumber = createString("cgRefNumber");

    public final DateTimePath<java.sql.Timestamp> createdTime = createDateTime("createdTime", java.sql.Timestamp.class);

    public final StringPath deviceLocalTxnTime = createString("deviceLocalTxnTime");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath merchantCode = createString("merchantCode");

    public final StringPath merchantName = createString("merchantName");

    public final NumberPath<Long> ticketTxnId = createNumber("ticketTxnId", Long.class);

    public final NumberPath<Long> totalAmount = createNumber("totalAmount", Long.class);

    public final NumberPath<Long> totalTxnAmount = createNumber("totalTxnAmount", Long.class);

    public final StringPath transactionType = createString("transactionType");

    public final DateTimePath<java.sql.Timestamp> txDdateTime = createDateTime("txDdateTime", java.sql.Timestamp.class);

    public final StringPath txnRefNumber = createString("txnRefNumber");

    public final StringPath txnStatus = createString("txnStatus");

    public QPgTransactionData(String variable) {
        super(PgTransactionData.class, forVariable(variable));
    }

    public QPgTransactionData(Path<? extends PgTransactionData> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPgTransactionData(PathMetadata<?> metadata) {
        super(PgTransactionData.class, metadata);
    }

}

