package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QVehicleTypeProfile is a Querydsl query type for VehicleTypeProfile
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QVehicleTypeProfile extends EntityPathBase<VehicleTypeProfile> {

    private static final long serialVersionUID = 34383888L;

    public static final QVehicleTypeProfile vehicleTypeProfile = new QVehicleTypeProfile("vehicleTypeProfile");

    public final StringPath description = createString("description");

    public final StringPath status = createString("status");

    public final StringPath vehicleType = createString("vehicleType");

    public final NumberPath<Long> vehicleTypeId = createNumber("vehicleTypeId", Long.class);

    public QVehicleTypeProfile(String variable) {
        super(VehicleTypeProfile.class, forVariable(variable));
    }

    public QVehicleTypeProfile(Path<? extends VehicleTypeProfile> path) {
        super(path.getType(), path.getMetadata());
    }

    public QVehicleTypeProfile(PathMetadata<?> metadata) {
        super(VehicleTypeProfile.class, metadata);
    }

}

