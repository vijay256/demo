package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QProductManagement is a Querydsl query type for ProductManagement
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QProductManagement extends EntityPathBase<ProductManagement> {

    private static final long serialVersionUID = 1768943397L;

    public static final QProductManagement productManagement = new QProductManagement("productManagement");

    public final NumberPath<Long> amount = createNumber("amount", Long.class);

    public final NumberPath<Long> discount = createNumber("discount", Long.class);

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> productId = createNumber("productId", Long.class);

    public final StringPath productName = createString("productName");

    public final StringPath productType = createString("productType");

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath reason = createString("reason");

    public final StringPath status = createString("status");

    public final StringPath ticketAndPassType = createString("ticketAndPassType");

    public final DateTimePath<java.sql.Timestamp> validFrom = createDateTime("validFrom", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> validTo = createDateTime("validTo", java.sql.Timestamp.class);

    public QProductManagement(String variable) {
        super(ProductManagement.class, forVariable(variable));
    }

    public QProductManagement(Path<? extends ProductManagement> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProductManagement(PathMetadata<?> metadata) {
        super(ProductManagement.class, metadata);
    }

}

