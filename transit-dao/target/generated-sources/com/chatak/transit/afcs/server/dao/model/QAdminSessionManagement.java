package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QAdminSessionManagement is a Querydsl query type for AdminSessionManagement
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAdminSessionManagement extends EntityPathBase<AdminSessionManagement> {

    private static final long serialVersionUID = 1414426423L;

    public static final QAdminSessionManagement adminSessionManagement = new QAdminSessionManagement("adminSessionManagement");

    public final StringPath deviceId = createString("deviceId");

    public final StringPath empId = createString("empId");

    public final DateTimePath<java.sql.Timestamp> generateDateAndTime = createDateTime("generateDateAndTime", java.sql.Timestamp.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final DateTimePath<java.sql.Timestamp> processDateAndtime = createDateTime("processDateAndtime", java.sql.Timestamp.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath transactionId = createString("transactionId");

    public final StringPath userId = createString("userId");

    public QAdminSessionManagement(String variable) {
        super(AdminSessionManagement.class, forVariable(variable));
    }

    public QAdminSessionManagement(Path<? extends AdminSessionManagement> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAdminSessionManagement(PathMetadata<?> metadata) {
        super(AdminSessionManagement.class, metadata);
    }

}

