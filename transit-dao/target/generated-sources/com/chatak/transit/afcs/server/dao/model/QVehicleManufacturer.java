package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QVehicleManufacturer is a Querydsl query type for VehicleManufacturer
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QVehicleManufacturer extends EntityPathBase<VehicleManufacturer> {

    private static final long serialVersionUID = -967788304L;

    public static final QVehicleManufacturer vehicleManufacturer = new QVehicleManufacturer("vehicleManufacturer");

    public final StringPath description = createString("description");

    public final StringPath status = createString("status");

    public final NumberPath<Integer> vehicleManufacturerId = createNumber("vehicleManufacturerId", Integer.class);

    public final StringPath vehicleManufacturerName = createString("vehicleManufacturerName");

    public final NumberPath<Long> vehicleTypeId = createNumber("vehicleTypeId", Long.class);

    public QVehicleManufacturer(String variable) {
        super(VehicleManufacturer.class, forVariable(variable));
    }

    public QVehicleManufacturer(Path<? extends VehicleManufacturer> path) {
        super(path.getType(), path.getMetadata());
    }

    public QVehicleManufacturer(PathMetadata<?> metadata) {
        super(VehicleManufacturer.class, metadata);
    }

}

