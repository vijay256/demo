package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QBatchSummary is a Querydsl query type for BatchSummary
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QBatchSummary extends EntityPathBase<BatchSummary> {

    private static final long serialVersionUID = -513280359L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QBatchSummary batchSummary = new QBatchSummary("batchSummary");

    public final NumberPath<Integer> batchNumber = createNumber("batchNumber", Integer.class);

    public final NumberPath<Integer> bsId = createNumber("bsId", Integer.class);

    public final NumberPath<Integer> cardAmount = createNumber("cardAmount", Integer.class);

    public final NumberPath<Integer> cardPaymentCount = createNumber("cardPaymentCount", Integer.class);

    public final NumberPath<Integer> cashAmount = createNumber("cashAmount", Integer.class);

    public final NumberPath<Integer> cashPaymentCount = createNumber("cashPaymentCount", Integer.class);

    public final NumberPath<Integer> deviceId = createNumber("deviceId", Integer.class);

    public final QOperator operator;

    public final NumberPath<Integer> shiftCode = createNumber("shiftCode", Integer.class);

    public final NumberPath<Integer> totalAmount = createNumber("totalAmount", Integer.class);

    public final NumberPath<Integer> totalCount = createNumber("totalCount", Integer.class);

    public QBatchSummary(String variable) {
        this(BatchSummary.class, forVariable(variable), INITS);
    }

    public QBatchSummary(Path<? extends BatchSummary> path) {
        this(path.getType(), path.getMetadata(), path.getMetadata().isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QBatchSummary(PathMetadata<?> metadata) {
        this(metadata, metadata.isRoot() ? INITS : PathInits.DEFAULT);
    }

    public QBatchSummary(PathMetadata<?> metadata, PathInits inits) {
        this(BatchSummary.class, metadata, inits);
    }

    public QBatchSummary(Class<? extends BatchSummary> type, PathMetadata<?> metadata, PathInits inits) {
        super(type, metadata, inits);
        this.operator = inits.isInitialized("operator") ? new QOperator(forProperty("operator")) : null;
    }

}

