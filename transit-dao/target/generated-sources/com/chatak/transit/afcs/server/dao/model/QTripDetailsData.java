package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QTripDetailsData is a Querydsl query type for TripDetailsData
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QTripDetailsData extends EntityPathBase<TripDetailsData> {

    private static final long serialVersionUID = 1076884730L;

    public static final QTripDetailsData tripDetailsData = new QTripDetailsData("tripDetailsData");

    public final NumberPath<Long> destinationStop = createNumber("destinationStop", Long.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> orginStop = createNumber("orginStop", Long.class);

    public final StringPath routecode = createString("routecode");

    public final NumberPath<Long> tripDetailId = createNumber("tripDetailId", Long.class);

    public final NumberPath<Long> tripnumber = createNumber("tripnumber", Long.class);

    public QTripDetailsData(String variable) {
        super(TripDetailsData.class, forVariable(variable));
    }

    public QTripDetailsData(Path<? extends TripDetailsData> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTripDetailsData(PathMetadata<?> metadata) {
        super(TripDetailsData.class, metadata);
    }

}

