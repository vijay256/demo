package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QFareMaster is a Querydsl query type for FareMaster
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QFareMaster extends EntityPathBase<FareMaster> {

    private static final long serialVersionUID = 443242237L;

    public static final QFareMaster fareMaster = new QFareMaster("fareMaster");

    public final StringPath difference = createString("difference");

    public final NumberPath<Double> fareAmount = createNumber("fareAmount", Double.class);

    public final NumberPath<Long> fareId = createNumber("fareId", Long.class);

    public final StringPath fareName = createString("fareName");

    public final StringPath fareType = createString("fareType");

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath reason = createString("reason");

    public final StringPath status = createString("status");

    public QFareMaster(String variable) {
        super(FareMaster.class, forVariable(variable));
    }

    public QFareMaster(Path<? extends FareMaster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFareMaster(PathMetadata<?> metadata) {
        super(FareMaster.class, metadata);
    }

}

