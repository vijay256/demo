package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QDeviceTracking is a Querydsl query type for DeviceTracking
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDeviceTracking extends EntityPathBase<DeviceTracking> {

    private static final long serialVersionUID = 1766031226L;

    public static final QDeviceTracking deviceTracking = new QDeviceTracking("deviceTracking");

    public final NumberPath<Long> deviceSerial = createNumber("deviceSerial", Long.class);

    public final NumberPath<Long> deviceTrackingId = createNumber("deviceTrackingId", Long.class);

    public final StringPath latitude = createString("latitude");

    public final StringPath longitude = createString("longitude");

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath routeId = createString("routeId");

    public final StringPath status = createString("status");

    public QDeviceTracking(String variable) {
        super(DeviceTracking.class, forVariable(variable));
    }

    public QDeviceTracking(Path<? extends DeviceTracking> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDeviceTracking(PathMetadata<?> metadata) {
        super(DeviceTracking.class, metadata);
    }

}

