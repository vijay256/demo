package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QEntityFeature is a Querydsl query type for EntityFeature
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QEntityFeature extends EntityPathBase<EntityFeature> {

    private static final long serialVersionUID = -822884442L;

    public static final QEntityFeature entityFeature = new QEntityFeature("entityFeature");

    public final NumberPath<Long> entityId = createNumber("entityId", Long.class);

    public final NumberPath<Long> featureId = createNumber("featureId", Long.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public QEntityFeature(String variable) {
        super(EntityFeature.class, forVariable(variable));
    }

    public QEntityFeature(Path<? extends EntityFeature> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEntityFeature(PathMetadata<?> metadata) {
        super(EntityFeature.class, metadata);
    }

}

