package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QDeviceManufacturerMaster is a Querydsl query type for DeviceManufacturerMaster
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDeviceManufacturerMaster extends EntityPathBase<DeviceManufacturerMaster> {

    private static final long serialVersionUID = -2103057770L;

    public static final QDeviceManufacturerMaster deviceManufacturerMaster = new QDeviceManufacturerMaster("deviceManufacturerMaster");

    public final StringPath createdBy = createString("createdBy");

    public final DateTimePath<java.sql.Timestamp> createdTime = createDateTime("createdTime", java.sql.Timestamp.class);

    public final StringPath description = createString("description");

    public final StringPath deviceManufacturer = createString("deviceManufacturer");

    public final NumberPath<Long> deviceManufacturerId = createNumber("deviceManufacturerId", Long.class);

    public final NumberPath<Long> deviceTypeId = createNumber("deviceTypeId", Long.class);

    public final StringPath status = createString("status");

    public final StringPath updatedBy = createString("updatedBy");

    public final DateTimePath<java.sql.Timestamp> updatedTime = createDateTime("updatedTime", java.sql.Timestamp.class);

    public QDeviceManufacturerMaster(String variable) {
        super(DeviceManufacturerMaster.class, forVariable(variable));
    }

    public QDeviceManufacturerMaster(Path<? extends DeviceManufacturerMaster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDeviceManufacturerMaster(PathMetadata<?> metadata) {
        super(DeviceManufacturerMaster.class, metadata);
    }

}

