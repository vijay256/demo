package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QVehicleOnboarding is a Querydsl query type for VehicleOnboarding
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QVehicleOnboarding extends EntityPathBase<VehicleOnboarding> {

    private static final long serialVersionUID = 8750842L;

    public static final QVehicleOnboarding vehicleOnboarding = new QVehicleOnboarding("vehicleOnboarding");

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath reason = createString("reason");

    public final StringPath status = createString("status");

    public final NumberPath<Integer> vehicleManufacturerId = createNumber("vehicleManufacturerId", Integer.class);

    public final NumberPath<Long> vehicleModelId = createNumber("vehicleModelId", Long.class);

    public final NumberPath<Long> vehicleOnboardingId = createNumber("vehicleOnboardingId", Long.class);

    public final NumberPath<Long> vehicleTypeId = createNumber("vehicleTypeId", Long.class);

    public QVehicleOnboarding(String variable) {
        super(VehicleOnboarding.class, forVariable(variable));
    }

    public QVehicleOnboarding(Path<? extends VehicleOnboarding> path) {
        super(path.getType(), path.getMetadata());
    }

    public QVehicleOnboarding(PathMetadata<?> metadata) {
        super(VehicleOnboarding.class, metadata);
    }

}

