package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QRouteMaster is a Querydsl query type for RouteMaster
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QRouteMaster extends EntityPathBase<RouteMaster> {

    private static final long serialVersionUID = 791790814L;

    public static final QRouteMaster routeMaster = new QRouteMaster("routeMaster");

    public final StringPath fromRoute = createString("fromRoute");

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath reason = createString("reason");

    public final StringPath routeCode = createString("routeCode");

    public final NumberPath<Long> routeId = createNumber("routeId", Long.class);

    public final StringPath routeName = createString("routeName");

    public final ListPath<StageMaster, QStageMaster> stageMap = this.<StageMaster, QStageMaster>createList("stageMap", StageMaster.class, QStageMaster.class, PathInits.DIRECT2);

    public final StringPath status = createString("status");

    public final StringPath toRoute = createString("toRoute");

    public QRouteMaster(String variable) {
        super(RouteMaster.class, forVariable(variable));
    }

    public QRouteMaster(Path<? extends RouteMaster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QRouteMaster(PathMetadata<?> metadata) {
        super(RouteMaster.class, metadata);
    }

}

