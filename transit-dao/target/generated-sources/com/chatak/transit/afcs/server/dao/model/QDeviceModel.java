package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QDeviceModel is a Querydsl query type for DeviceModel
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDeviceModel extends EntityPathBase<DeviceModel> {

    private static final long serialVersionUID = 1534396742L;

    public static final QDeviceModel deviceModel = new QDeviceModel("deviceModel");

    public final StringPath createdBy = createString("createdBy");

    public final DateTimePath<java.sql.Timestamp> createdTime = createDateTime("createdTime", java.sql.Timestamp.class);

    public final StringPath description = createString("description");

    public final NumberPath<Long> deviceId = createNumber("deviceId", Long.class);

    public final StringPath deviceIMEINumber = createString("deviceIMEINumber");

    public final NumberPath<Long> deviceManufacturerId = createNumber("deviceManufacturerId", Long.class);

    public final StringPath deviceModelName = createString("deviceModelName");

    public final NumberPath<Long> deviceTypeId = createNumber("deviceTypeId", Long.class);

    public final StringPath reason = createString("reason");

    public final StringPath status = createString("status");

    public final StringPath updatedBy = createString("updatedBy");

    public final DateTimePath<java.sql.Timestamp> updatedTime = createDateTime("updatedTime", java.sql.Timestamp.class);

    public QDeviceModel(String variable) {
        super(DeviceModel.class, forVariable(variable));
    }

    public QDeviceModel(Path<? extends DeviceModel> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDeviceModel(PathMetadata<?> metadata) {
        super(DeviceModel.class, metadata);
    }

}

