package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QDeviceOnboardMaster is a Querydsl query type for DeviceOnboardMaster
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDeviceOnboardMaster extends EntityPathBase<DeviceOnboardMaster> {

    private static final long serialVersionUID = 1437745862L;

    public static final QDeviceOnboardMaster deviceOnboardMaster = new QDeviceOnboardMaster("deviceOnboardMaster");

    public final NumberPath<Long> deviceManufacturerId = createNumber("deviceManufacturerId", Long.class);

    public final NumberPath<Long> deviceModelId = createNumber("deviceModelId", Long.class);

    public final NumberPath<Long> deviceOnboardId = createNumber("deviceOnboardId", Long.class);

    public final NumberPath<Long> deviceTypeId = createNumber("deviceTypeId", Long.class);

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath reason = createString("reason");

    public final StringPath status = createString("status");

    public QDeviceOnboardMaster(String variable) {
        super(DeviceOnboardMaster.class, forVariable(variable));
    }

    public QDeviceOnboardMaster(Path<? extends DeviceOnboardMaster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDeviceOnboardMaster(PathMetadata<?> metadata) {
        super(DeviceOnboardMaster.class, metadata);
    }

}

