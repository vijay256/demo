package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QFeeMaster is a Querydsl query type for FeeMaster
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QFeeMaster extends EntityPathBase<FeeMaster> {

    private static final long serialVersionUID = -925133541L;

    public static final QFeeMaster feeMaster = new QFeeMaster("feeMaster");

    public final NumberPath<Long> feeId = createNumber("feeId", Long.class);

    public final StringPath feeName = createString("feeName");

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final StringPath orgFeeType = createString("orgFeeType");

    public final StringPath orgFeeValue = createString("orgFeeValue");

    public final StringPath orgShareType = createString("orgShareType");

    public final StringPath orgShareValue = createString("orgShareValue");

    public final StringPath ptoFeeType = createString("ptoFeeType");

    public final StringPath ptoFeeValue = createString("ptoFeeValue");

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath ptoShareType = createString("ptoShareType");

    public final StringPath ptoShareValue = createString("ptoShareValue");

    public final StringPath reason = createString("reason");

    public final StringPath status = createString("status");

    public QFeeMaster(String variable) {
        super(FeeMaster.class, forVariable(variable));
    }

    public QFeeMaster(Path<? extends FeeMaster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFeeMaster(PathMetadata<?> metadata) {
        super(FeeMaster.class, metadata);
    }

}

