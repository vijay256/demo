package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QTransitMasterMaintenance is a Querydsl query type for TransitMasterMaintenance
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QTransitMasterMaintenance extends EntityPathBase<TransitMasterMaintenance> {

    private static final long serialVersionUID = 836248011L;

    public static final QTransitMasterMaintenance transitMasterMaintenance = new QTransitMasterMaintenance("transitMasterMaintenance");

    public final DateTimePath<java.sql.Timestamp> applyDate = createDateTime("applyDate", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> deliveryDate = createDateTime("deliveryDate", java.sql.Timestamp.class);

    public final StringPath description = createString("description");

    public final StringPath fullVersion = createString("fullVersion");

    public final StringPath inheritt = createString("inheritt");

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath reason = createString("reason");

    public final StringPath status = createString("status");

    public final NumberPath<Long> transitMasterId = createNumber("transitMasterId", Long.class);

    public final StringPath versionNumber = createString("versionNumber");

    public QTransitMasterMaintenance(String variable) {
        super(TransitMasterMaintenance.class, forVariable(variable));
    }

    public QTransitMasterMaintenance(Path<? extends TransitMasterMaintenance> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTransitMasterMaintenance(PathMetadata<?> metadata) {
        super(TransitMasterMaintenance.class, metadata);
    }

}

