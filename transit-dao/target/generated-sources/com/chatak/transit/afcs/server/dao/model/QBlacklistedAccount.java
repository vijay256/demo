package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QBlacklistedAccount is a Querydsl query type for BlacklistedAccount
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QBlacklistedAccount extends EntityPathBase<BlacklistedAccount> {

    private static final long serialVersionUID = 177529310L;

    public static final QBlacklistedAccount blacklistedAccount = new QBlacklistedAccount("blacklistedAccount");

    public final StringPath account = createString("account");

    public final NumberPath<Long> blacklistedId = createNumber("blacklistedId", Long.class);

    public final NumberPath<Long> issuerId = createNumber("issuerId", Long.class);

    public final StringPath issuerName = createString("issuerName");

    public final StringPath pto = createString("pto");

    public final StringPath reason = createString("reason");

    public QBlacklistedAccount(String variable) {
        super(BlacklistedAccount.class, forVariable(variable));
    }

    public QBlacklistedAccount(Path<? extends BlacklistedAccount> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBlacklistedAccount(PathMetadata<?> metadata) {
        super(BlacklistedAccount.class, metadata);
    }

}

