package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QEmailMaster is a Querydsl query type for EmailMaster
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QEmailMaster extends EntityPathBase<EmailMaster> {

    private static final long serialVersionUID = 491246577L;

    public static final QEmailMaster emailMaster = new QEmailMaster("emailMaster");

    public final DateTimePath<java.sql.Timestamp> createdDate = createDateTime("createdDate", java.sql.Timestamp.class);

    public final StringPath emailAddress = createString("emailAddress");

    public final ArrayPath<byte[], Byte> emailBody = createArray("emailBody", byte[].class);

    public final NumberPath<Long> emailId = createNumber("emailId", Long.class);

    public final StringPath eventType = createString("eventType");

    public final DateTimePath<java.sql.Timestamp> scheduledDate = createDateTime("scheduledDate", java.sql.Timestamp.class);

    public final StringPath status = createString("status");

    public final StringPath subject = createString("subject");

    public final DateTimePath<java.sql.Timestamp> updatedDate = createDateTime("updatedDate", java.sql.Timestamp.class);

    public QEmailMaster(String variable) {
        super(EmailMaster.class, forVariable(variable));
    }

    public QEmailMaster(Path<? extends EmailMaster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEmailMaster(PathMetadata<?> metadata) {
        super(EmailMaster.class, metadata);
    }

}

