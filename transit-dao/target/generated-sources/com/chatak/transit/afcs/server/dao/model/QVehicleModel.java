package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QVehicleModel is a Querydsl query type for VehicleModel
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QVehicleModel extends EntityPathBase<VehicleModel> {

    private static final long serialVersionUID = -620012822L;

    public static final QVehicleModel vehicleModel = new QVehicleModel("vehicleModel");

    public final StringPath description = createString("description");

    public final StringPath reason = createString("reason");

    public final StringPath status = createString("status");

    public final StringPath vehicleChassisNumber = createString("vehicleChassisNumber");

    public final StringPath vehicleEngineNumber = createString("vehicleEngineNumber");

    public final NumberPath<Integer> vehicleManufacturerId = createNumber("vehicleManufacturerId", Integer.class);

    public final NumberPath<Long> vehicleModelId = createNumber("vehicleModelId", Long.class);

    public final StringPath vehicleModelName = createString("vehicleModelName");

    public final StringPath vehicleRegistrationNumber = createString("vehicleRegistrationNumber");

    public final NumberPath<Long> vehicleTypeId = createNumber("vehicleTypeId", Long.class);

    public QVehicleModel(String variable) {
        super(VehicleModel.class, forVariable(variable));
    }

    public QVehicleModel(Path<? extends VehicleModel> path) {
        super(path.getType(), path.getMetadata());
    }

    public QVehicleModel(PathMetadata<?> metadata) {
        super(VehicleModel.class, metadata);
    }

}

