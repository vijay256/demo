package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPtoMaster is a Querydsl query type for PtoMaster
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPtoMaster extends EntityPathBase<PtoMaster> {

    private static final long serialVersionUID = -1162355520L;

    public static final QPtoMaster ptoMaster = new QPtoMaster("ptoMaster");

    public final StringPath city = createString("city");

    public final StringPath contactPerson = createString("contactPerson");

    public final StringPath createdBy = createString("createdBy");

    public final DateTimePath<java.sql.Timestamp> createdDateTime = createDateTime("createdDateTime", java.sql.Timestamp.class);

    public final NumberPath<Long> orgId = createNumber("orgId", Long.class);

    public final StringPath ptoEmail = createString("ptoEmail");

    public final NumberPath<Long> ptoMasterId = createNumber("ptoMasterId", Long.class);

    public final StringPath ptoMobile = createString("ptoMobile");

    public final StringPath ptoName = createString("ptoName");

    public final StringPath reason = createString("reason");

    public final StringPath siteUrl = createString("siteUrl");

    public final StringPath state = createString("state");

    public final StringPath status = createString("status");

    public final StringPath updatedBy = createString("updatedBy");

    public final DateTimePath<java.sql.Timestamp> updatedDateTime = createDateTime("updatedDateTime", java.sql.Timestamp.class);

    public final StringPath userId = createString("userId");

    public QPtoMaster(String variable) {
        super(PtoMaster.class, forVariable(variable));
    }

    public QPtoMaster(Path<? extends PtoMaster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPtoMaster(PathMetadata<?> metadata) {
        super(PtoMaster.class, metadata);
    }

}

