package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QDiscountMaster is a Querydsl query type for DiscountMaster
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QDiscountMaster extends EntityPathBase<DiscountMaster> {

    private static final long serialVersionUID = 453536592L;

    public static final QDiscountMaster discountMaster = new QDiscountMaster("discountMaster");

    public final NumberPath<Long> discount = createNumber("discount", Long.class);

    public final NumberPath<Long> discountId = createNumber("discountId", Long.class);

    public final StringPath discountName = createString("discountName");

    public final StringPath discountType = createString("discountType");

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath reason = createString("reason");

    public final NumberPath<Long> routeStageStationDifference = createNumber("routeStageStationDifference", Long.class);

    public final StringPath status = createString("status");

    public QDiscountMaster(String variable) {
        super(DiscountMaster.class, forVariable(variable));
    }

    public QDiscountMaster(Path<? extends DiscountMaster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QDiscountMaster(PathMetadata<?> metadata) {
        super(DiscountMaster.class, metadata);
    }

}

