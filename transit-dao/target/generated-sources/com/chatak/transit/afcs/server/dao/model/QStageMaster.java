package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QStageMaster is a Querydsl query type for StageMaster
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QStageMaster extends EntityPathBase<StageMaster> {

    private static final long serialVersionUID = -1380075053L;

    public static final QStageMaster stageMaster = new QStageMaster("stageMaster");

    public final NumberPath<Double> distance = createNumber("distance", Double.class);

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath reason = createString("reason");

    public final NumberPath<Long> routeId = createNumber("routeId", Long.class);

    public final NumberPath<Long> stageId = createNumber("stageId", Long.class);

    public final StringPath stageName = createString("stageName");

    public final NumberPath<Integer> stageSequenceNumber = createNumber("stageSequenceNumber", Integer.class);

    public final StringPath status = createString("status");

    public final ListPath<StopProfile, QStopProfile> stopMap = this.<StopProfile, QStopProfile>createList("stopMap", StopProfile.class, QStopProfile.class, PathInits.DIRECT2);

    public QStageMaster(String variable) {
        super(StageMaster.class, forVariable(variable));
    }

    public QStageMaster(Path<? extends StageMaster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QStageMaster(PathMetadata<?> metadata) {
        super(StageMaster.class, metadata);
    }

}

