package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QBulkUploadDetails is a Querydsl query type for BulkUploadDetails
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QBulkUploadDetails extends EntityPathBase<BulkUploadDetails> {

    private static final long serialVersionUID = 320700834L;

    public static final QBulkUploadDetails bulkUploadDetails = new QBulkUploadDetails("bulkUploadDetails");

    public final StringPath endStopCode = createString("endStopCode");

    public final StringPath fareAmount = createString("fareAmount");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath routeCode = createString("routeCode");

    public final StringPath startStopCode = createString("startStopCode");

    public QBulkUploadDetails(String variable) {
        super(BulkUploadDetails.class, forVariable(variable));
    }

    public QBulkUploadDetails(Path<? extends BulkUploadDetails> path) {
        super(path.getType(), path.getMetadata());
    }

    public QBulkUploadDetails(PathMetadata<?> metadata) {
        super(BulkUploadDetails.class, metadata);
    }

}

