package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QOperator is a Querydsl query type for Operator
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QOperator extends EntityPathBase<Operator> {

    private static final long serialVersionUID = -179094863L;

    public static final QOperator operator = new QOperator("operator");

    public final SetPath<BatchSummary, QBatchSummary> batchSummaryData = this.<BatchSummary, QBatchSummary>createSet("batchSummaryData", BatchSummary.class, QBatchSummary.class, PathInits.DIRECT2);

    public final StringPath operatorContactNumber = createString("operatorContactNumber");

    public final NumberPath<Long> operatorId = createNumber("operatorId", Long.class);

    public final StringPath operatorName = createString("operatorName");

    public final StringPath operatorPassword = createString("operatorPassword");

    public final StringPath operatorUserId = createString("operatorUserId");

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath reason = createString("reason");

    public final StringPath status = createString("status");

    public QOperator(String variable) {
        super(Operator.class, forVariable(variable));
    }

    public QOperator(Path<? extends Operator> path) {
        super(path.getType(), path.getMetadata());
    }

    public QOperator(PathMetadata<?> metadata) {
        super(Operator.class, metadata);
    }

}

