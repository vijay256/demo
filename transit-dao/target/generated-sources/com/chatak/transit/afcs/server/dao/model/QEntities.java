package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QEntities is a Querydsl query type for Entities
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QEntities extends EntityPathBase<Entities> {

    private static final long serialVersionUID = -1780655666L;

    public static final QEntities entities = new QEntities("entities");

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public QEntities(String variable) {
        super(Entities.class, forVariable(variable));
    }

    public QEntities(Path<? extends Entities> path) {
        super(path.getType(), path.getMetadata());
    }

    public QEntities(PathMetadata<?> metadata) {
        super(Entities.class, metadata);
    }

}

