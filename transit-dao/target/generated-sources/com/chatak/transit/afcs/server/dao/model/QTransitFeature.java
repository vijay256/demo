package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QTransitFeature is a Querydsl query type for TransitFeature
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QTransitFeature extends EntityPathBase<TransitFeature> {

    private static final long serialVersionUID = 1805913584L;

    public static final QTransitFeature transitFeature = new QTransitFeature("transitFeature");

    public final DateTimePath<java.sql.Timestamp> createdDate = createDateTime("createdDate", java.sql.Timestamp.class);

    public final NumberPath<Long> featureId = createNumber("featureId", Long.class);

    public final NumberPath<Long> featureLevel = createNumber("featureLevel", Long.class);

    public final StringPath name = createString("name");

    public final NumberPath<Long> refFeatureId = createNumber("refFeatureId", Long.class);

    public final StringPath roleType = createString("roleType");

    public final StringPath status = createString("status");

    public QTransitFeature(String variable) {
        super(TransitFeature.class, forVariable(variable));
    }

    public QTransitFeature(Path<? extends TransitFeature> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTransitFeature(PathMetadata<?> metadata) {
        super(TransitFeature.class, metadata);
    }

}

