package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QFinancialTransactionData is a Querydsl query type for FinancialTransactionData
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QFinancialTransactionData extends EntityPathBase<FinancialTransactionData> {

    private static final long serialVersionUID = -1882912788L;

    public static final QFinancialTransactionData financialTransactionData = new QFinancialTransactionData("financialTransactionData");

    public final NumberPath<Double> amount0 = createNumber("amount0", Double.class);

    public final NumberPath<Double> amount1 = createNumber("amount1", Double.class);

    public final NumberPath<Double> amount10 = createNumber("amount10", Double.class);

    public final NumberPath<Double> amount11 = createNumber("amount11", Double.class);

    public final NumberPath<Double> amount12 = createNumber("amount12", Double.class);

    public final NumberPath<Double> amount13 = createNumber("amount13", Double.class);

    public final NumberPath<Double> amount14 = createNumber("amount14", Double.class);

    public final NumberPath<Double> amount15 = createNumber("amount15", Double.class);

    public final NumberPath<Double> amount16 = createNumber("amount16", Double.class);

    public final NumberPath<Double> amount17 = createNumber("amount17", Double.class);

    public final NumberPath<Double> amount18 = createNumber("amount18", Double.class);

    public final NumberPath<Double> amount19 = createNumber("amount19", Double.class);

    public final NumberPath<Double> amount2 = createNumber("amount2", Double.class);

    public final NumberPath<Double> amount20 = createNumber("amount20", Double.class);

    public final NumberPath<Double> amount21 = createNumber("amount21", Double.class);

    public final NumberPath<Double> amount22 = createNumber("amount22", Double.class);

    public final NumberPath<Double> amount23 = createNumber("amount23", Double.class);

    public final NumberPath<Double> amount24 = createNumber("amount24", Double.class);

    public final NumberPath<Double> amount25 = createNumber("amount25", Double.class);

    public final NumberPath<Double> amount26 = createNumber("amount26", Double.class);

    public final NumberPath<Double> amount27 = createNumber("amount27", Double.class);

    public final NumberPath<Double> amount28 = createNumber("amount28", Double.class);

    public final NumberPath<Double> amount29 = createNumber("amount29", Double.class);

    public final NumberPath<Double> amount3 = createNumber("amount3", Double.class);

    public final NumberPath<Double> amount30 = createNumber("amount30", Double.class);

    public final NumberPath<Double> amount31 = createNumber("amount31", Double.class);

    public final NumberPath<Double> amount32 = createNumber("amount32", Double.class);

    public final NumberPath<Double> amount33 = createNumber("amount33", Double.class);

    public final NumberPath<Double> amount34 = createNumber("amount34", Double.class);

    public final NumberPath<Double> amount35 = createNumber("amount35", Double.class);

    public final NumberPath<Double> amount36 = createNumber("amount36", Double.class);

    public final NumberPath<Double> amount37 = createNumber("amount37", Double.class);

    public final NumberPath<Double> amount38 = createNumber("amount38", Double.class);

    public final NumberPath<Double> amount39 = createNumber("amount39", Double.class);

    public final NumberPath<Double> amount4 = createNumber("amount4", Double.class);

    public final NumberPath<Double> amount40 = createNumber("amount40", Double.class);

    public final NumberPath<Double> amount41 = createNumber("amount41", Double.class);

    public final NumberPath<Double> amount42 = createNumber("amount42", Double.class);

    public final NumberPath<Double> amount43 = createNumber("amount43", Double.class);

    public final NumberPath<Double> amount44 = createNumber("amount44", Double.class);

    public final NumberPath<Double> amount45 = createNumber("amount45", Double.class);

    public final NumberPath<Double> amount46 = createNumber("amount46", Double.class);

    public final NumberPath<Double> amount47 = createNumber("amount47", Double.class);

    public final NumberPath<Double> amount48 = createNumber("amount48", Double.class);

    public final NumberPath<Double> amount49 = createNumber("amount49", Double.class);

    public final NumberPath<Double> amount5 = createNumber("amount5", Double.class);

    public final NumberPath<Double> amount6 = createNumber("amount6", Double.class);

    public final NumberPath<Double> amount7 = createNumber("amount7", Double.class);

    public final NumberPath<Double> amount8 = createNumber("amount8", Double.class);

    public final NumberPath<Double> amount9 = createNumber("amount9", Double.class);

    public final NumberPath<Integer> count0 = createNumber("count0", Integer.class);

    public final NumberPath<Integer> count1 = createNumber("count1", Integer.class);

    public final NumberPath<Integer> count10 = createNumber("count10", Integer.class);

    public final NumberPath<Integer> count11 = createNumber("count11", Integer.class);

    public final NumberPath<Integer> count12 = createNumber("count12", Integer.class);

    public final NumberPath<Integer> count13 = createNumber("count13", Integer.class);

    public final NumberPath<Integer> count14 = createNumber("count14", Integer.class);

    public final NumberPath<Integer> count15 = createNumber("count15", Integer.class);

    public final NumberPath<Integer> count16 = createNumber("count16", Integer.class);

    public final NumberPath<Integer> count17 = createNumber("count17", Integer.class);

    public final NumberPath<Integer> count18 = createNumber("count18", Integer.class);

    public final NumberPath<Integer> count19 = createNumber("count19", Integer.class);

    public final NumberPath<Integer> count2 = createNumber("count2", Integer.class);

    public final NumberPath<Integer> count20 = createNumber("count20", Integer.class);

    public final NumberPath<Integer> count21 = createNumber("count21", Integer.class);

    public final NumberPath<Integer> count22 = createNumber("count22", Integer.class);

    public final NumberPath<Integer> count23 = createNumber("count23", Integer.class);

    public final NumberPath<Integer> count24 = createNumber("count24", Integer.class);

    public final NumberPath<Integer> count25 = createNumber("count25", Integer.class);

    public final NumberPath<Integer> count26 = createNumber("count26", Integer.class);

    public final NumberPath<Integer> count27 = createNumber("count27", Integer.class);

    public final NumberPath<Integer> count28 = createNumber("count28", Integer.class);

    public final NumberPath<Integer> count29 = createNumber("count29", Integer.class);

    public final NumberPath<Integer> count3 = createNumber("count3", Integer.class);

    public final NumberPath<Integer> count30 = createNumber("count30", Integer.class);

    public final NumberPath<Integer> count31 = createNumber("count31", Integer.class);

    public final NumberPath<Integer> count32 = createNumber("count32", Integer.class);

    public final NumberPath<Integer> count33 = createNumber("count33", Integer.class);

    public final NumberPath<Integer> count34 = createNumber("count34", Integer.class);

    public final NumberPath<Integer> count35 = createNumber("count35", Integer.class);

    public final NumberPath<Integer> count36 = createNumber("count36", Integer.class);

    public final NumberPath<Integer> count37 = createNumber("count37", Integer.class);

    public final NumberPath<Integer> count38 = createNumber("count38", Integer.class);

    public final NumberPath<Integer> count39 = createNumber("count39", Integer.class);

    public final NumberPath<Integer> count4 = createNumber("count4", Integer.class);

    public final NumberPath<Integer> count40 = createNumber("count40", Integer.class);

    public final NumberPath<Integer> count41 = createNumber("count41", Integer.class);

    public final NumberPath<Integer> count42 = createNumber("count42", Integer.class);

    public final NumberPath<Integer> count43 = createNumber("count43", Integer.class);

    public final NumberPath<Integer> count44 = createNumber("count44", Integer.class);

    public final NumberPath<Integer> count45 = createNumber("count45", Integer.class);

    public final NumberPath<Integer> count46 = createNumber("count46", Integer.class);

    public final NumberPath<Integer> count47 = createNumber("count47", Integer.class);

    public final NumberPath<Integer> count48 = createNumber("count48", Integer.class);

    public final NumberPath<Integer> count49 = createNumber("count49", Integer.class);

    public final NumberPath<Integer> count5 = createNumber("count5", Integer.class);

    public final NumberPath<Integer> count6 = createNumber("count6", Integer.class);

    public final NumberPath<Integer> count7 = createNumber("count7", Integer.class);

    public final NumberPath<Integer> count8 = createNumber("count8", Integer.class);

    public final NumberPath<Integer> count9 = createNumber("count9", Integer.class);

    public final DateTimePath<java.sql.Timestamp> createdDateTime = createDateTime("createdDateTime", java.sql.Timestamp.class);

    public final StringPath dateTime = createString("dateTime");

    public final StringPath deviceComponentVersion = createString("deviceComponentVersion");

    public final StringPath deviceId = createString("deviceId");

    public final StringPath deviceModelNo = createString("deviceModelNo");

    public final StringPath deviceSerialNo = createString("deviceSerialNo");

    public final StringPath financialTxnType = createString("financialTxnType");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath masterVersion = createString("masterVersion");

    public final StringPath paymentHostBatchNo = createString("paymentHostBatchNo");

    public final StringPath paymentHostTid = createString("paymentHostTid");

    public final StringPath ptoOperationId = createString("ptoOperationId");

    public final NumberPath<Integer> shiftBatchNo = createNumber("shiftBatchNo", Integer.class);

    public final StringPath shiftCode = createString("shiftCode");

    public final StringPath softwareVersion = createString("softwareVersion");

    public final StringPath tripNo = createString("tripNo");

    public final DateTimePath<java.sql.Timestamp> updatedDateTime = createDateTime("updatedDateTime", java.sql.Timestamp.class);

    public final StringPath userId = createString("userId");

    public QFinancialTransactionData(String variable) {
        super(FinancialTransactionData.class, forVariable(variable));
    }

    public QFinancialTransactionData(Path<? extends FinancialTransactionData> path) {
        super(path.getType(), path.getMetadata());
    }

    public QFinancialTransactionData(PathMetadata<?> metadata) {
        super(FinancialTransactionData.class, metadata);
    }

}

