package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QPaymentGateway is a Querydsl query type for PaymentGateway
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QPaymentGateway extends EntityPathBase<PaymentGateway> {

    private static final long serialVersionUID = -374993493L;

    public static final QPaymentGateway paymentGateway = new QPaymentGateway("paymentGateway");

    public final NumberPath<Integer> cityId = createNumber("cityId", Integer.class);

    public final NumberPath<Integer> countryId = createNumber("countryId", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath pgName = createString("pgName");

    public final NumberPath<Long> ptoMasterId = createNumber("ptoMasterId", Long.class);

    public final StringPath serviceUrl = createString("serviceUrl");

    public final NumberPath<Integer> stateId = createNumber("stateId", Integer.class);

    public QPaymentGateway(String variable) {
        super(PaymentGateway.class, forVariable(variable));
    }

    public QPaymentGateway(Path<? extends PaymentGateway> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPaymentGateway(PathMetadata<?> metadata) {
        super(PaymentGateway.class, metadata);
    }

}

