package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;
import com.mysema.query.types.path.PathInits;


/**
 * QTicketsTxnData is a Querydsl query type for TicketsTxnData
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QTicketsTxnData extends EntityPathBase<TicketsTxnData> {

    private static final long serialVersionUID = -93465446L;

    public static final QTicketsTxnData ticketsTxnData = new QTicketsTxnData("ticketsTxnData");

    public final StringPath cardBalance = createString("cardBalance");

    public final StringPath cardExpiryDate = createString("cardExpiryDate");

    public final StringPath cardNumber = createString("cardNumber");

    public final StringPath checksum = createString("checksum");

    public final StringPath conductorEmpId = createString("conductorEmpId");

    public final StringPath currentStopId = createString("currentStopId");

    public final NumberPath<Long> deviceId = createNumber("deviceId", Long.class);

    public final StringPath driverEmpId = createString("driverEmpId");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath operatorId = createString("operatorId");

    public final StringPath passTypeCode = createString("passTypeCode");

    public final StringPath paymentTxnTerminalId = createString("paymentTxnTerminalId");

    public final StringPath paymentTxnUniqueId = createString("paymentTxnUniqueId");

    public final SetPath<PgTransactionData, QPgTransactionData> pgTransactionData = this.<PgTransactionData, QPgTransactionData>createSet("pgTransactionData", PgTransactionData.class, QPgTransactionData.class, PathInits.DIRECT2);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath routeCode = createString("routeCode");

    public final NumberPath<Integer> shiftBatchNumber = createNumber("shiftBatchNumber", Integer.class);

    public final StringPath shiftCode = createString("shiftCode");

    public final DateTimePath<java.sql.Timestamp> ticketDateTime = createDateTime("ticketDateTime", java.sql.Timestamp.class);

    public final StringPath ticketDestStationCode = createString("ticketDestStationCode");

    public final NumberPath<Double> ticketFareAmount = createNumber("ticketFareAmount", Double.class);

    public final StringPath ticketFareOptionalNegativeAmount = createString("ticketFareOptionalNegativeAmount");

    public final StringPath ticketFareOptionalPositiveAmount = createString("ticketFareOptionalPositiveAmount");

    public final StringPath ticketNumber = createString("ticketNumber");

    public final DatePath<java.sql.Date> ticketOperationDate = createDate("ticketOperationDate", java.sql.Date.class);

    public final StringPath ticketOriginStationCode = createString("ticketOriginStationCode");

    public final StringPath ticketPassengerCount = createString("ticketPassengerCount");

    public final StringPath ticketPaymentMode = createString("ticketPaymentMode");

    public final StringPath transactionId = createString("transactionId");

    public final StringPath transportID = createString("transportID");

    public final SetPath<TripDetailsData, QTripDetailsData> tripDetailsData = this.<TripDetailsData, QTripDetailsData>createSet("tripDetailsData", TripDetailsData.class, QTripDetailsData.class, PathInits.DIRECT2);

    public final StringPath tripNumber = createString("tripNumber");

    public final StringPath txnSts = createString("txnSts");

    public QTicketsTxnData(String variable) {
        super(TicketsTxnData.class, forVariable(variable));
    }

    public QTicketsTxnData(Path<? extends TicketsTxnData> path) {
        super(path.getType(), path.getMetadata());
    }

    public QTicketsTxnData(PathMetadata<?> metadata) {
        super(TicketsTxnData.class, metadata);
    }

}

