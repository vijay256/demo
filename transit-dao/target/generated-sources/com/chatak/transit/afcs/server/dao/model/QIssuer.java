package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QIssuer is a Querydsl query type for Issuer
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QIssuer extends EntityPathBase<Issuer> {

    private static final long serialVersionUID = -1473797114L;

    public static final QIssuer issuer = new QIssuer("issuer");

    public final NumberPath<Integer> cityId = createNumber("cityId", Integer.class);

    public final NumberPath<Integer> countryId = createNumber("countryId", Integer.class);

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath issuerName = createString("issuerName");

    public final NumberPath<Long> ptoMaterId = createNumber("ptoMaterId", Long.class);

    public final StringPath serviceUrl = createString("serviceUrl");

    public final NumberPath<Integer> stateId = createNumber("stateId", Integer.class);

    public QIssuer(String variable) {
        super(Issuer.class, forVariable(variable));
    }

    public QIssuer(Path<? extends Issuer> path) {
        super(path.getType(), path.getMetadata());
    }

    public QIssuer(PathMetadata<?> metadata) {
        super(Issuer.class, metadata);
    }

}

