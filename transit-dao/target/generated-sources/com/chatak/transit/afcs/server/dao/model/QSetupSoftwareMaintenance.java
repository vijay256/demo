package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QSetupSoftwareMaintenance is a Querydsl query type for SetupSoftwareMaintenance
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QSetupSoftwareMaintenance extends EntityPathBase<SetupSoftwareMaintenance> {

    private static final long serialVersionUID = -2045915844L;

    public static final QSetupSoftwareMaintenance setupSoftwareMaintenance = new QSetupSoftwareMaintenance("setupSoftwareMaintenance");

    public final DateTimePath<java.sql.Timestamp> applyDate = createDateTime("applyDate", java.sql.Timestamp.class);

    public final StringPath createdBy = createString("createdBy");

    public final DateTimePath<java.sql.Timestamp> createdTime = createDateTime("createdTime", java.sql.Timestamp.class);

    public final DateTimePath<java.sql.Timestamp> deliveryDate = createDateTime("deliveryDate", java.sql.Timestamp.class);

    public final StringPath description = createString("description");

    public final StringPath fullVersion = createString("fullVersion");

    public final StringPath inheritt = createString("inheritt");

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final NumberPath<Long> softwareId = createNumber("softwareId", Long.class);

    public final StringPath status = createString("status");

    public final StringPath updatedBy = createString("updatedBy");

    public final DateTimePath<java.sql.Timestamp> updatedTime = createDateTime("updatedTime", java.sql.Timestamp.class);

    public final StringPath versionNumber = createString("versionNumber");

    public QSetupSoftwareMaintenance(String variable) {
        super(SetupSoftwareMaintenance.class, forVariable(variable));
    }

    public QSetupSoftwareMaintenance(Path<? extends SetupSoftwareMaintenance> path) {
        super(path.getType(), path.getMetadata());
    }

    public QSetupSoftwareMaintenance(PathMetadata<?> metadata) {
        super(SetupSoftwareMaintenance.class, metadata);
    }

}

