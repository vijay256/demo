package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QAfcsRole is a Querydsl query type for AfcsRole
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QAfcsRole extends EntityPathBase<AfcsRole> {

    private static final long serialVersionUID = 849475448L;

    public static final QAfcsRole afcsRole = new QAfcsRole("afcsRole");

    public final StringPath createdBy = createString("createdBy");

    public final DateTimePath<java.sql.Timestamp> createdDate = createDateTime("createdDate", java.sql.Timestamp.class);

    public final StringPath description = createString("description");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath name = createString("name");

    public final StringPath reason = createString("reason");

    public final StringPath status = createString("status");

    public final StringPath updatedBy = createString("updatedBy");

    public final DateTimePath<java.sql.Timestamp> updatedDate = createDateTime("updatedDate", java.sql.Timestamp.class);

    public final StringPath userType = createString("userType");

    public QAfcsRole(String variable) {
        super(AfcsRole.class, forVariable(variable));
    }

    public QAfcsRole(Path<? extends AfcsRole> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAfcsRole(PathMetadata<?> metadata) {
        super(AfcsRole.class, metadata);
    }

}

