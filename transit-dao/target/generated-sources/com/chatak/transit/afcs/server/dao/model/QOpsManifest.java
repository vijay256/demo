package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QOpsManifest is a Querydsl query type for OpsManifest
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QOpsManifest extends EntityPathBase<OpsManifest> {

    private static final long serialVersionUID = -837286444L;

    public static final QOpsManifest opsManifest = new QOpsManifest("opsManifest");

    public final DateTimePath<java.sql.Timestamp> date = createDateTime("date", java.sql.Timestamp.class);

    public final NumberPath<Long> depotCode = createNumber("depotCode", Long.class);

    public final NumberPath<Long> depotId = createNumber("depotId", Long.class);

    public final NumberPath<Long> deviceNo = createNumber("deviceNo", Long.class);

    public final NumberPath<Long> operatorId = createNumber("operatorId", Long.class);

    public final NumberPath<Long> opsManifestId = createNumber("opsManifestId", Long.class);

    public final NumberPath<Long> organizationId = createNumber("organizationId", Long.class);

    public final NumberPath<Long> ptoId = createNumber("ptoId", Long.class);

    public final StringPath status = createString("status");

    public QOpsManifest(String variable) {
        super(OpsManifest.class, forVariable(variable));
    }

    public QOpsManifest(Path<? extends OpsManifest> path) {
        super(path.getType(), path.getMetadata());
    }

    public QOpsManifest(PathMetadata<?> metadata) {
        super(OpsManifest.class, metadata);
    }

}

