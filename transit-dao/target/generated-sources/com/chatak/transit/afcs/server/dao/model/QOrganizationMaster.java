package com.chatak.transit.afcs.server.dao.model;

import static com.mysema.query.types.PathMetadataFactory.*;

import com.mysema.query.types.path.*;

import com.mysema.query.types.PathMetadata;
import javax.annotation.Generated;
import com.mysema.query.types.Path;


/**
 * QOrganizationMaster is a Querydsl query type for OrganizationMaster
 */
@Generated("com.mysema.query.codegen.EntitySerializer")
public class QOrganizationMaster extends EntityPathBase<OrganizationMaster> {

    private static final long serialVersionUID = 1392949058L;

    public static final QOrganizationMaster organizationMaster = new QOrganizationMaster("organizationMaster");

    public final StringPath city = createString("city");

    public final StringPath contactPerson = createString("contactPerson");

    public final StringPath createdBy = createString("createdBy");

    public final DateTimePath<java.sql.Timestamp> createdDateTime = createDateTime("createdDateTime", java.sql.Timestamp.class);

    public final StringPath organizationEmail = createString("organizationEmail");

    public final StringPath organizationMobileNumber = createString("organizationMobileNumber");

    public final StringPath organizationName = createString("organizationName");

    public final NumberPath<Long> orgId = createNumber("orgId", Long.class);

    public final StringPath reason = createString("reason");

    public final StringPath siteUrl = createString("siteUrl");

    public final StringPath state = createString("state");

    public final StringPath status = createString("status");

    public final StringPath updatedBy = createString("updatedBy");

    public final DateTimePath<java.sql.Timestamp> updatedDateTime = createDateTime("updatedDateTime", java.sql.Timestamp.class);

    public QOrganizationMaster(String variable) {
        super(OrganizationMaster.class, forVariable(variable));
    }

    public QOrganizationMaster(Path<? extends OrganizationMaster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QOrganizationMaster(PathMetadata<?> metadata) {
        super(OrganizationMaster.class, metadata);
    }

}

