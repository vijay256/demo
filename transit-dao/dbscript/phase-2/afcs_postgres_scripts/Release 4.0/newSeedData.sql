INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7000, 'Interface', 0, 'SUPER_ADMIN', 0, 'Active', '2018-09-12 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7001, 'Issuer', 1, 'SUPER_ADMIN', 7000, 'Active', '2018-09-12 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7002, 'Create', 2, 'SUPER_ADMIN', 7001, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7003, 'Edit', 2, 'SUPER_ADMIN', 7001, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7004, 'View', 2, 'SUPER_ADMIN', 7001, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7005, 'Terminate', 2, 'SUPER_ADMIN', 7001, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7006, 'Suspend', 2, 'SUPER_ADMIN', 7001, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7007, 'Activate', 2, 'SUPER_ADMIN', 7001, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7008, 'Search', 2, 'SUPER_ADMIN', 7001, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7009, 'Payment gateway', 1, 'SUPER_ADMIN', 7000, 'Active', '2018-09-12 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7010, 'Create', 2, 'SUPER_ADMIN', 7009, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7011, 'Edit', 2, 'SUPER_ADMIN', 7009, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7012, 'View', 2, 'SUPER_ADMIN', 7009, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7013, 'Terminate', 2, 'SUPER_ADMIN', 7009, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7014, 'Suspend', 2, 'SUPER_ADMIN', 7009, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7015, 'Activate', 2, 'SUPER_ADMIN', 7009, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7016, 'Search', 2, 'SUPER_ADMIN', 7009, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7017, 'Report', 0, 'SUPER_ADMIN', 0, 'Active', '2018-09-12 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7018, 'Transaction Data Report', 1, 'SUPER_ADMIN', 7017, 'Active', '2018-09-12 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7019, 'Generate', 2, 'SUPER_ADMIN', 7018, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7020, 'Search', 2, 'SUPER_ADMIN', 7018, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7021, 'Operator Session Report', 1, 'SUPER_ADMIN', 7017, 'Active', '2018-09-12 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7022, 'Generate', 2, 'SUPER_ADMIN', 7021, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7023, 'Search', 2, 'SUPER_ADMIN', 7021, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7024, 'Passenger Analysis', 1, 'SUPER_ADMIN', 7017, 'Active', '2018-09-12 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7025, 'Generate', 2, 'SUPER_ADMIN', 7024, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7026, 'Search', 2, 'SUPER_ADMIN', 7024, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7027, 'PTO Summary Report', 1, 'SUPER_ADMIN', 7017, 'Active', '2018-09-12 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7028, 'Generate', 2, 'SUPER_ADMIN', 7027, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7029, 'Search', 2, 'SUPER_ADMIN', 7027, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7030, 'Transaction Report', 1, 'SUPER_ADMIN', 7017, 'Active', '2018-09-12 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7031, 'Generate', 2, 'SUPER_ADMIN', 7030, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7032, 'Search', 2, 'SUPER_ADMIN', 7030, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date)
VALUES(7033, 'Master File Download', 1, 'SUPER_ADMIN', 7017, 'Active', '2018-09-12 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7034, 'Generate', 2, 'SUPER_ADMIN', 7033, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7035, 'Search', 2, 'SUPER_ADMIN', 7033, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7036, 'Device tracking', 0, 'SUPER_ADMIN', 0, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7037, 'Settings', 0, 'SUPER_ADMIN', 0, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7038, 'Change Password', 1, 'SUPER_ADMIN', 7037, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7039, 'Edit', 2, 'SUPER_ADMIN', 7038, 'Active', '2019-05-18 02:02:27.000');
INSERT INTO transit_feature (feature_id, "name", feature_level, role_type, ref_feature_id, status, created_date) 
VALUES(7040, 'Black Listed Card', 1, 'SUPER_ADMIN', 1002, 'Active', '2019-05-18 02:02:27.000');

INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(602,1, 7000);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(603,2, 7000);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(604,3, 7000);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(605, 1, 7001);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(606, 2, 7001);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(607, 3, 7001);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(608,1, 7002);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(609,2, 7002);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(610,3, 7002);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(611,1, 7003);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(612,2, 7003);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(613,3, 7003);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(614,1, 7004);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(615,2, 7004);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(616,3, 7004);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(617,1, 7005);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(618,2, 7005);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(619,3, 7005);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(620,1, 7006);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(621,2, 7006);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(622,3, 7006);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(623,1, 7007);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(624,2, 7007);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(625,3, 7007);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(626,1, 7008);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(627,2, 7008);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(628,3, 7008);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(629,1, 7009);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(630,2, 7009);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(631,3, 7009);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(632, 1, 7010);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(633, 2, 7010);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(634, 3, 7010);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(635,1, 7011);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(636,2, 7011);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(637,3, 7011);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(638,1, 7012);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(639,2, 7012);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(640,3, 7012);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(641,1, 7013);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(642,2, 7013);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(643,3, 7013);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(644,1, 7014);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(645,2, 7014);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(646,3, 7014);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(647,1, 7015);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(648,2, 7015);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(649,3, 7015);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(650,1, 7016);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(651,2, 7016);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(652,3, 7016);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(653,1, 7017);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(654,2, 7017);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(655,3, 7017);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(656,1, 7018);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(657,2, 7018);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(658,3, 7018);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(659,1, 7019);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(660,2, 7019);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(661,3, 7019);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(662,1, 7020);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(663,2, 7020);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(664,3, 7020);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(665,1, 7021);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(666,2, 7021);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(667,3, 7021);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(668,1, 7022);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(669,2, 7022);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(670,3, 7022);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(671,1, 7023);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(672,2, 7023);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(673,3, 7023);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(674,1, 7024);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(675,2, 7024);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(676,3, 7024);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(677,1, 7025);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(678,2, 7025);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(679,3, 7025);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(680,1, 7026);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(681,2, 7026);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(682,3, 7026);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(683,1, 7027);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(684,2, 7027);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(685,3, 7027);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(686,1, 7028);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(687,2, 7028);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(688,3, 7028);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(689,1, 7029);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(690,2, 7029);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(691,3, 7029);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(692,1, 7030);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(693,2, 7030);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(694,3, 7030);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(695,1, 7031);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(696,2, 7031);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(697,3, 7031);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(698,1, 7032);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(699,2, 7032);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(700,3, 7032);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(701,1, 7033);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(702,2, 7033);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(703,3, 7033);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(704,1, 7034);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(705,2, 7034);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(706,3, 7034);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(707,1, 7035);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(708,2, 7035);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(709,3, 7035);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(710,1, 7036);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(711,2, 7036);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(712,3, 7036);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(713,1, 7037);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(714,2, 7037);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(715,3, 7037);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(716,1, 7038);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(717,2, 7038);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(718,3, 7038);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(719,1, 7039);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(720,2, 7039);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(721,3, 7039);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(722,1, 7040);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(723,2, 7040);
INSERT INTO entity_feature (id, entity_id, feature_id) VALUES(724,3, 7040);

INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45400,1, 7000, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45401,1, 7001, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45402,1, 7002, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45403,1, 7003, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45404,1, 7004, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45405,1, 7005, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45406,1, 7006, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45407,1, 7007, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45408,1, 7008, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45409,2, 7000, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45410,2, 7001, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45411,2, 7002, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45412,2, 7003, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45413,2, 7004, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45414,2, 7005, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45415,2, 7006, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45416,2, 7007, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45417,2, 7008, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45418,3, 7000, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45419,3, 7001, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45420,3, 7002, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45421,3, 7003, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45422,3, 7004, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45423,3, 7005, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45424,3, 7006, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45425,3, 7007, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45426,3, 7008, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45427,1, 7009, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45428,1, 7010, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45429,1, 7011, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45430,1, 7012, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45431,1, 7013, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45432,1, 7014, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45433,1, 7015, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45434,1, 7016, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45435,2, 7009, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45436,2, 7010, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45437,2, 7011, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45438,2, 7012, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45439,2, 7013, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45440,2, 7014, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45441,2, 7015, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45442,2, 7016, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45443,3, 7009, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45444,3, 7010, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45445,3, 7011, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45446,3, 7012, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45447,3, 7013, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45448,3, 7014, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45449,3, 7015, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45450,3, 7016, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45451,1, 7017, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45452,1, 7018, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45453,1, 7019, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45454,1, 7020, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45455,1, 7021, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45456,1, 7022, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45457,1, 7023, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45458,1, 7024, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45459,1, 7025, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45460,1, 7026, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45461,1, 7027, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45462,1, 7028, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45463,1, 7029, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45464,1, 7030, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45465,1, 7031, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45466,1, 7032, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45467,1, 7033, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45468,1, 7034, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45469,1, 7035, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45470,2, 7017, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45471,2, 7018, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45472,2, 7019, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45473,2, 7020, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45474,2, 7021, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45475,2, 7022, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45476,2, 7023, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45477,2, 7024, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45478,2, 7025, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45479,2, 7026, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45480,2, 7027, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45481,2, 7028, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45482,2, 7029, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45483,2, 7030, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45484,2, 7031, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45485,2, 7032, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45486,2, 7033, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45487,2, 7034, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45488,2, 7035, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45489,3, 7017, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45490,3, 7018, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45491,3, 7019, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45492,3, 7020, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45493,3, 7021, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45494,3, 7022, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45495,3, 7023, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45496,3, 7024, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45497,3, 7025, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45498,3, 7026, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45499,3, 7027, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45500,3, 7028, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45501,3, 7029, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45502,3, 7030, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45503,3, 7031, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45504,3, 7032, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45505,3, 7033, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45506,3, 7034, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45507,3, 7035, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45508,1, 7036, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45509,1, 7037, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45510,1, 7038, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45511,1, 7039, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45512,2, 7036, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45513,2, 7037, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45514,2, 7038, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45515,2, 7039, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45516,3, 7036, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45517,3, 7037, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45518,3, 7038, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45519,3, 7039, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);




INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45520,1, 7040, 'SUPER_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45521,2, 7040, 'ORG_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);
INSERT INTO portal_role_feature_map (role_feature_map_id, user_role_id, feature_id, created_by, created_date, updated_by, updated_date, user_type) 
VALUES(45522,3, 7040, 'PTO_ADMIN', '2019-05-18 16:43:09.000', NULL, NULL, NULL);