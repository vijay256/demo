package com.chatak.transit.afcs.server.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.chatak.transit.afcs.server.dao.model.LoginSessionDetails;

public interface LoginSessionDetailsRepository extends JpaRepository<LoginSessionDetails, Long>{

}
